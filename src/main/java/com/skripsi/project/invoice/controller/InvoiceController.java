package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.InvoiceItem;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.form.InvoiceUpdateForm;
import com.skripsi.project.invoice.form.InvoiceUpdateForm;
import com.skripsi.project.invoice.form.EmpolyeeAddForm;
import com.skripsi.project.invoice.form.InvoiceAddForm;
import com.skripsi.project.invoice.service.*;
import com.skripsi.project.invoice.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.OutputStream;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class InvoiceController {

    private final InvoiceItemService invoiceItemService;
    
    private final InvoiceService invoiceService;

    private final ProjectService projectService;

    private final ModelMapper modelMapper;

    private final InvoiceReportService invoiceReportService;

    @GetMapping("/invoice/list")
    public ModelAndView invoiceListGet() {
        ModelAndView modelAndView = new ModelAndView("invoice");

        return modelAndView;
    }

    @GetMapping("/invoice/add")
    public ModelAndView invoiceAddGet() {
        ModelAndView modelAndView = new ModelAndView("invoice-add");

        InvoiceAddForm invoiceAddForm = new InvoiceAddForm();
        invoiceAddForm.setNoInvoice("INV-"+ Instant.now().toEpochMilli());
        modelAndView.addObject("projectList", projectService.findAll());
        modelAndView.addObject("invoiceAddForm", invoiceAddForm);
        modelAndView.addObject("invoiceStatus", buildStatus());
        modelAndView.addObject("invoiceSkema", buildSkema());

        return modelAndView;
    }

    @PostMapping("/invoice/add")
    public ModelAndView invoiceAddPost(@Valid InvoiceAddForm request, BindingResult result, HttpServletRequest httpServletRequest) {
        ModelAndView modelAndView = new ModelAndView("redirect:/invoice/list");

        Invoice invoice = modelMapper.map(request,Invoice.class);
        invoice.setId(null);
        if (request.getIdProject() != null) {
            invoice.setProject(projectService.findById(request.getIdProject()));
        }

        invoiceService.save(invoice);

        modelAndView.setViewName("redirect:/invoice/update/" + invoice.getId());

        return modelAndView;
    }

    @GetMapping("/invoice/update/{id}")
    public ModelAndView invoiceUpdateGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("invoice-update");

        Invoice invoice = invoiceService.findById(id);

        if (invoice == null){
            modelAndView.setViewName("redirect:/invoice/list");
            return modelAndView;
        }

        InvoiceUpdateForm invoiceUpdateForm = modelMapper.map(invoice,InvoiceUpdateForm.class);
        invoiceUpdateForm.setIdProject(ObjectUtils.isEmpty(invoice.getProject()) ? null:invoice.getProject().getId());
        log.debug("updateInvoice: {}", invoiceUpdateForm);

        List<InvoiceItem> invoiceItemList = invoiceItemService.findAllByInvoiceId(invoice.getId());

        modelAndView.addObject("projectList", projectService.findAll());
        modelAndView.addObject("invoiceUpdateForm",invoiceUpdateForm);
        modelAndView.addObject("invoiceItemList",invoiceItemList);
        modelAndView.addObject("invoiceStatus", buildStatus());
        modelAndView.addObject("invoiceSkema", buildSkema());

        return modelAndView;
    }

    @PostMapping("/invoice/update/{id}")
    public ModelAndView invoiceUpdatePost(@PathVariable("id") Long id, @Valid InvoiceUpdateForm invoiceUpdateForm, BindingResult result, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView("redirect:/invoice/list");

        if (!result.hasErrors()){
            log.debug(invoiceUpdateForm.toString());
            Invoice invoice = modelMapper.map(invoiceUpdateForm,Invoice.class);
            if (invoiceUpdateForm.getIdProject() != null) {
                invoice.setProject(projectService.findById(invoiceUpdateForm.getIdProject()));
            }
            invoiceService.save(invoice);
        }else {
            modelAndView.setViewName("redirect:/invoice/update/" + id);

            modelAndView.addObject("invoiceUpdateForm", invoiceUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/invoice/delete/{id}")
    public ModelAndView invoiceAddGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("redirect:/invoice/list");
        
        invoiceService.deleteById(id);

        return modelAndView;
    }

    @GetMapping("/invoice/print/{id}")
    public void printReport (@PathVariable("id") Long id, HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"invoice_%s.pdf\"",id));

        OutputStream out = response.getOutputStream();
        JasperPrint invoiceReport = invoiceReportService.printReport(id);
        JasperExportManager.exportReportToPdfStream(invoiceReport,out);

    }


    private List<String> buildStatus() {
        return Arrays.asList(
                "PAID",
                "UNPAID"
        );
    }

    private List<String> buildSkema() {
        return Arrays.asList(
                "FULL",
                "DP",
                "Termin",
                "Pelunasan"
        );
    }
}
