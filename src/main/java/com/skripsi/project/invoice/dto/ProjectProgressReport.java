package com.skripsi.project.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectProgressReport implements Serializable {

    private String noProject;

    private String title;

    private String client;

    private String location;

    private String pic;

    private String nospk;

    private LocalDate startDate;

    private LocalDate endDate;

    private String status;


}
