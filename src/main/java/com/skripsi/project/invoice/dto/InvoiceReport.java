package com.skripsi.project.invoice.dto;

import com.skripsi.project.invoice.entity.Project;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceReport implements Serializable {

    private Long id;

    private Project project;

    private String noInvoice;

    private String status;

    private String address;

    private LocalDate invoiceDate;

    private BigDecimal ppn;

    private BigDecimal ppnAmount;

    private BigDecimal subtotal;

    private BigDecimal grandtotal;

    private List<InvoiceItemReport> invoiceItemReports = new ArrayList<>();

}
