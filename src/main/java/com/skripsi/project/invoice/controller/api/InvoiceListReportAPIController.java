package com.skripsi.project.invoice.controller.api;

import com.skripsi.project.invoice.service.InvoiceListReportService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.time.LocalDate;

@Slf4j
@RestController
@RequestMapping("/api")
public class InvoiceListReportAPIController {

    @Autowired
    private InvoiceListReportService invoiceListReportService;

    @GetMapping("/invoice/list/report")
    public void printReport (@RequestParam("startDate") String start,
                             @RequestParam("endDate") String end,
                             HttpServletResponse response) throws Exception {
        response.setContentType("application/x-download");
        response.setHeader("Content-Disposition", String.format("attachment; filaname=\"invoice_report-%s%s.pdf\"",start,end));

        LocalDate startDate = ObjectUtils.isEmpty(start) ? null:LocalDate.parse(start);
        LocalDate endDate = ObjectUtils.isEmpty(end) ? null:LocalDate.parse(end);
        OutputStream out = response.getOutputStream();
        JasperPrint invoiceReport = invoiceListReportService.printReport(startDate,endDate);
        JasperExportManager.exportReportToPdfStream(invoiceReport,out);

    }
}
