package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.InvoiceItem;
import com.skripsi.project.invoice.form.InvoiceItemAddForm;
import com.skripsi.project.invoice.form.InvoiceItemUpdateForm;
import com.skripsi.project.invoice.form.InvoiceUpdateForm;
import com.skripsi.project.invoice.service.InvoiceItemService;
import com.skripsi.project.invoice.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;

@Slf4j
@Controller
@RequiredArgsConstructor
public class InvoiceItemController {
    
    private final InvoiceItemService invoiceItemService;

    private final InvoiceService invoiceService;

    private final ModelMapper modelMapper;

    @GetMapping("/invoice-item/list")
    public ModelAndView invoiceItemListGet() {
        ModelAndView modelAndView = new ModelAndView("invoice-item");

        return modelAndView;
    }

    @GetMapping("/invoice-item/add/invoice/{id}")
    public ModelAndView invoiceItemAddGet(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("invoice-item-add");

        InvoiceItemAddForm invoiceItemAddForm = new InvoiceItemAddForm();
        invoiceItemAddForm.setIdInvoice(id);
        modelAndView.addObject("invoiceItemAddForm", new InvoiceItemAddForm());
        modelAndView.addObject("idInvoice", invoiceItemAddForm.getIdInvoice());

        return modelAndView;
    }

    @PostMapping("/invoice-item/add/invoice/{id}")
    public ModelAndView invoiceItemAddPost(@PathVariable("id") Long id, @Valid InvoiceItemAddForm request, BindingResult result, HttpServletRequest httpServletRequest) {
        ModelAndView modelAndView = new ModelAndView();
        log.debug("invoiceItemAddPost: {}",request);

        modelAndView.setViewName("invoice-item-add");

        Invoice invoice = invoiceService.findById(id);
        InvoiceItem invoiceItem = modelMapper.map(request,InvoiceItem.class);
        invoiceItem.setId(null);
        invoiceItem.setInvoice(invoice);

        InvoiceItem item = invoiceItemService.save(invoiceItem);

        if (!ObjectUtils.isEmpty(item.getErrorMessage())) {
            modelAndView.setViewName("invoice-item-add");
            modelAndView.addObject("invoiceItemAddForm", request);
            modelAndView.addObject("idInvoice", invoice.getId());
            modelAndView.addObject("errorMessage",item.getErrorMessage());
            return modelAndView;
        }

        modelAndView.setViewName("redirect:/invoice/update/" + invoice.getId());

        return modelAndView;
    }

    @GetMapping("/invoice-item/update/{id}")
    public ModelAndView invoiceItemUpdateGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("invoice-item-update");

        InvoiceItem invoiceItem = invoiceItemService.findById(id);

        if (invoiceItem == null){
            modelAndView.setViewName("redirect:/invoice/update/" + invoiceItem.getInvoice().getId());
            return modelAndView;
        }

        InvoiceItemUpdateForm invoiceItemUpdateForm = modelMapper.map(invoiceItem,InvoiceItemUpdateForm.class);
        invoiceItemUpdateForm.setIdInvoice(invoiceItem.getInvoice().getId());

        modelAndView.addObject("invoiceItemUpdateForm", invoiceItemUpdateForm);
        modelAndView.addObject("idInvoice", invoiceItemUpdateForm.getIdInvoice());

        return modelAndView;
    }

    @PostMapping("/invoice-item/update/{id}")
    public ModelAndView invoiceItemUpdatePost(@PathVariable("id") Long id, @Valid InvoiceItemUpdateForm invoiceItemUpdateForm, BindingResult result, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        InvoiceItem data = invoiceItemService.findById(id);

        if (!result.hasErrors()){
            log.debug("invoiceItemUpdatePost: {}",invoiceItemUpdateForm.toString());
            InvoiceItem invoiceItem = modelMapper.map(invoiceItemUpdateForm,InvoiceItem.class);
            invoiceItem.setInvoice(data.getInvoice());
            modelAndView.setViewName("redirect:/invoice/update/" + data.getInvoice().getId());

            InvoiceItem item = invoiceItemService.save(invoiceItem);

            if (!ObjectUtils.isEmpty(item.getErrorMessage())) {
                modelAndView.setViewName("invoice-item-add");
                modelAndView.addObject("invoiceItemAddForm", invoiceItemUpdateForm);
                modelAndView.addObject("idInvoice", data.getInvoice().getId());
                modelAndView.addObject("errorMessage",item.getErrorMessage());
                return modelAndView;
            }

        }else {
            modelAndView.setViewName("invoice-item-update");
            invoiceItemUpdateForm.setIdInvoice(data.getInvoice().getId());

            modelAndView.addObject("invoiceItemUpdateForm", invoiceItemUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/invoice-item/delete/{id}")
    public ModelAndView invoiceItemDeleteGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("redirect:/invoice/list");

        invoiceItemService.deleteById(id);

        return modelAndView;
    }
}
