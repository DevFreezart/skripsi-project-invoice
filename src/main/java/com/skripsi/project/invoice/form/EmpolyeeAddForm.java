package com.skripsi.project.invoice.form;

import com.skripsi.project.invoice.entity.Position;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmpolyeeAddForm {

    private Long positionId;

    private String name;

    private String nip;

    private String password;

}
