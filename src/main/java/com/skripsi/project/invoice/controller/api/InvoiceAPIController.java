package com.skripsi.project.invoice.controller.api;

import com.skripsi.project.invoice.domain.pagination.DataTableRequest;
import com.skripsi.project.invoice.domain.pagination.DataTableResults;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.service.InvoiceService;
import com.skripsi.project.invoice.utils.AppUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class InvoiceAPIController {

    @Autowired
    private InvoiceService invoiceService;

    @RequestMapping(value = "/invoice/list", method = RequestMethod.GET, params = {"datatables"}, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataTableResults historyListDatatables(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        DataTableRequest<Invoice> dataTableRequest = new DataTableRequest(httpServletRequest);

        int page = dataTableRequest.getPage();
        int limit = dataTableRequest.getLength();

        String start = AppUtil.getParameter(httpServletRequest,"startDate", "");
        String end = AppUtil.getParameter(httpServletRequest,"endDate", "");
        log.debug("startDate: {}, endDate: {}", start,end);
        LocalDate startDate = ObjectUtils.isEmpty(start) ? null:LocalDate.parse(start);
        LocalDate endDate = ObjectUtils.isEmpty(end) ? null:LocalDate.parse(end);

        String search = (dataTableRequest.getSearch() == null ? "" : dataTableRequest.getSearch());

        String sortDirection = dataTableRequest.getOrder().getSortDir();

        Pageable pageable = null;

        if (sortDirection.equalsIgnoreCase("DESC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "noInvoice"));
        } else if (sortDirection.equalsIgnoreCase("ASC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "noInvoice"));
        } else {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "noInvoice"));

        }

        Page<Invoice> pageInvoice;
        pageInvoice = invoiceService.findAllByNo(search, pageable,startDate,endDate);

        List<Invoice> invoiceList = pageInvoice.getContent();

        DataTableResults<Invoice> dataTableResults = new DataTableResults<Invoice>();

        dataTableResults.setListOfDataObjects(invoiceList);
        dataTableResults.setDraw(dataTableRequest.getDraw());
        if (!invoiceList.isEmpty()) {
            dataTableResults.setRecordsTotal(String.valueOf(pageInvoice.getTotalElements()));
            dataTableResults.setRecordsFiltered(String.valueOf(pageInvoice.getTotalElements()));
        }

        return dataTableResults;
    }
}
