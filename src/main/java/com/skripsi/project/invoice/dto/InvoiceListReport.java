package com.skripsi.project.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceListReport implements Serializable {

    private String projectName;

    private String spk;

    private String skema;

    private String noInvoice;

    private LocalDate tanggalInvoice;

    private String status;

}
