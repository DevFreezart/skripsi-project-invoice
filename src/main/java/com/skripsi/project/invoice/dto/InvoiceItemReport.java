package com.skripsi.project.invoice.dto;

import com.skripsi.project.invoice.entity.Invoice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceItemReport implements Serializable {

    private Long id;

    private String deskripsi;

    private String satuan;

    private Integer volume;

    private BigDecimal hargasatuan;

    private BigDecimal hargajumlah;

}
