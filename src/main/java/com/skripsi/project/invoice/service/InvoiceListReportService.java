package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.dto.InvoiceListReport;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InvoiceListReportService {

    private final InvoiceRepository invoiceRepository;

    public JasperPrint printReport(LocalDate startDate, LocalDate endDatee) throws Exception {

        List<InvoiceListReport> invoiceReports = buildDataReport(startDate,endDatee);

        JRDataSource ds = new JRBeanCollectionDataSource(invoiceReports);

        Resource report = new ClassPathResource("templates/report/invoice_report.jrxml");
        Resource logoSrc = new ClassPathResource("static/img/logo.png");

        Map<String, Object> parameters = new HashMap<>();
        BufferedImage logo = ImageIO.read(logoSrc.getURL());
        Locale locale = new Locale("id", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", locale);
        String start = ObjectUtils.isEmpty(startDate) ? "":formatter.format(startDate);
        String end = ObjectUtils.isEmpty(endDatee) ? "":formatter.format(endDatee);

        parameters.put("tglStart", start);
        parameters.put("tglEnd", end);
        parameters.put("logo",logo);

        JasperReport jasperReport = JasperCompileManager.compileReport(report.getInputStream());

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);

        return jasperPrint;

    }

    private List<InvoiceListReport> buildDataReport(LocalDate startDate, LocalDate endDatee) {

        List<Invoice> invoiceList = new ArrayList<>();

        if (startDate ==  null && endDatee == null) {
            invoiceList = invoiceRepository.findAll();
        } else if (endDatee == null) {
            invoiceList = invoiceRepository.findByInvoiceDateGreaterThanEqual(startDate);
        } else if (startDate == null) {
            invoiceList = invoiceRepository.findByInvoiceDateLessThanEqual(endDatee);
        } else {
            invoiceList = invoiceRepository.findByInvoiceDateGreaterThanEqualAndInvoiceDateLessThanEqual(startDate,endDatee);
        }

        List<InvoiceListReport> report = invoiceList.stream()
                .map(
                        invoice -> {
                            Project project = invoice.getProject();
                            if (project == null) {
                                project = new Project();
                            }
                            return InvoiceListReport.builder()
                                    .projectName(project.getTitle())
                                    .spk(project.getNospk())
                                    .skema(invoice.getSkema())
                                    .noInvoice(invoice.getNoInvoice())
                                    .tanggalInvoice(invoice.getInvoiceDate())
                                    .status(invoice.getStatus())
                                    .build();
                        }
                ).collect(Collectors.toList());

        if (report.isEmpty()) {
            report = Arrays.asList(InvoiceListReport.builder().build());
        }

        return report;

    }

}
