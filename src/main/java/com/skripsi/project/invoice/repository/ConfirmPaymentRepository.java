package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.ConfirmPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmPaymentRepository extends JpaRepository<ConfirmPayment,Long>, JpaSpecificationExecutor<ConfirmPayment> {

    Optional<ConfirmPayment> findByInvoiceId(Long id);

    void deleteAllByInvoiceId(Long id);

}
