package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.InvoiceItem;
import com.skripsi.project.invoice.repository.InvoiceItemRepository;
import com.skripsi.project.invoice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InvoiceItemService {

    private final InvoiceItemRepository invoiceItemRepository;

    private final InvoiceRepository invoiceRepository;

    public List<InvoiceItem> findAllByInvoiceId(Long invoiceId) {
        return invoiceItemRepository.findAllByInvoiceId(invoiceId);
    }

    public InvoiceItem findById(Long id) {
        Optional<InvoiceItem> checkInvoiceItem = invoiceItemRepository.findById(id);
        if (checkInvoiceItem.isPresent()) {
            return checkInvoiceItem.get();
        }

        return null;
    }

    @Transactional
    public InvoiceItem save(InvoiceItem item) {
        BigDecimal hargaJumlah = item.getHargasatuan().multiply(BigDecimal.valueOf(item.getVolume()));

        //cek total harga item dengan existing

        if (item.getId() == null) {
            BigDecimal totalHargaExisting = Optional.ofNullable(invoiceItemRepository.getTotalHarga(item.getInvoice().getId())).orElse(BigDecimal.ZERO);
            BigDecimal newTotalHarga = totalHargaExisting.add(hargaJumlah);
            BigDecimal hargaInvoice = item.getInvoice().getHarga();
            if (hargaInvoice.compareTo(newTotalHarga) < 0) {
                item.setErrorMessage("Gagal tambah/ubah data. Total harga item terlalu besar, terdapat selisih " + hargaInvoice.subtract(newTotalHarga));
                return item;
            }
        } else {

            BigDecimal totalHargaExisting = Optional.ofNullable(invoiceItemRepository.getTotalHargaWitoutItemId(item.getInvoice().getId(),item.getId())).orElse(BigDecimal.ZERO);
            BigDecimal newTotalHarga = totalHargaExisting.add(hargaJumlah);
            BigDecimal hargaInvoice = item.getInvoice().getHarga();
            if (hargaInvoice.compareTo(newTotalHarga) < 0) {
                item.setErrorMessage("Gagal tambah/ubah data. Total harga item terlalu besar, terdapat selisih " + hargaInvoice.subtract(newTotalHarga));
                return item;
            }
        }

        item.setHargajumlah(hargaJumlah);
        invoiceItemRepository.save(item);
        Invoice invoice = item.getInvoice();
        BigDecimal subTotal = invoiceItemRepository.getTotalHarga(invoice.getId());
        BigDecimal ppn = invoice.getPpn() == null ? BigDecimal.ZERO : invoice.getPpn();
        invoice.setSubtotal(subTotal == null ?BigDecimal.ZERO:subTotal);
        BigDecimal ppnAmount = (ppn.divide(BigDecimal.valueOf(100))).multiply(invoice.getSubtotal());
        invoice.setPpnAmount(ppnAmount);
        invoice.setGrandtotal(invoice.getSubtotal().add(ppnAmount));
        invoiceRepository.save(invoice);

        return invoiceItemRepository.save(item);
    }

    public void deleteById(Long id) {
        invoiceItemRepository.deleteById(id);
    }

    public BigDecimal getTotalHarga(Long invoiceId) {
        return invoiceItemRepository.getTotalHarga(invoiceId);
    }

}
