package com.skripsi.project.invoice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "invoice_item")
public class InvoiceItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_invoice")
    private Invoice invoice;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "uom", length = 10)
    private String satuan;

    @Column(name = "quantity")
    private Integer volume;

    @Column(name = "price")
    private BigDecimal hargasatuan;

    @Column(name = "subtotal")
    private BigDecimal hargajumlah;

    @Transient
    private String errorMessage;

}
