package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Position;
import com.skripsi.project.invoice.repository.PositionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PositionService {

    private final PositionRepository projectRepository;

    public List<Position> findAll() {
        return projectRepository.findAll();
    }

    public Page<Position> findAllByTitle(String title, Pageable pageable) {
        return projectRepository.findAllByTitleLike("%"+title+"%",pageable);
    }

    public Position findById(Long id) {
        Optional<Position> checkPosition =  projectRepository.findById(id);
        if (checkPosition.isPresent()) {
            return checkPosition.get();
        }

        return null;
    }

    public Position save(Position project) {
        return projectRepository.save(project);
    }

    public void deleteById(Long id) {
        projectRepository.deleteById(id);
    }

}
