package com.skripsi.project.invoice.controller.api;

import com.skripsi.project.invoice.domain.pagination.DataTableRequest;
import com.skripsi.project.invoice.domain.pagination.DataTableResults;
import com.skripsi.project.invoice.entity.Employee;
import com.skripsi.project.invoice.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class EmployeeAPIController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/employee/list", method = RequestMethod.GET, params = {"datatables"}, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataTableResults historyListDatatables(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        DataTableRequest<Employee> dataTableRequest = new DataTableRequest(httpServletRequest);

        int page = dataTableRequest.getPage();
        int limit = dataTableRequest.getLength();

        String search = (dataTableRequest.getSearch() == null ? "" : dataTableRequest.getSearch());

        String sortDirection = dataTableRequest.getOrder().getSortDir();

        Pageable pageable = null;

        if (sortDirection.equalsIgnoreCase("DESC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "nip"));
        } else if (sortDirection.equalsIgnoreCase("ASC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "nip"));
        } else {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "nip"));

        }

        Page<Employee> pageEmployee;
        pageEmployee = employeeService.findAllByNip(search, pageable);

        List<Employee> employeeList = pageEmployee.getContent();

        DataTableResults<Employee> dataTableResults = new DataTableResults<Employee>();

        dataTableResults.setListOfDataObjects(employeeList);
        dataTableResults.setDraw(dataTableRequest.getDraw());
        if (!employeeList.isEmpty()) {
            dataTableResults.setRecordsTotal(String.valueOf(pageEmployee.getTotalElements()));
            dataTableResults.setRecordsFiltered(String.valueOf(pageEmployee.getTotalElements()));
        }

        return dataTableResults;
    }
}
