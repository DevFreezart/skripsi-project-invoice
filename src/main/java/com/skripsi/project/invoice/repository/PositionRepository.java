package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<Position,Long>, JpaSpecificationExecutor<Position> {

    Page<Position> findAllByTitleLike(String noPosition, Pageable pageable);

}
