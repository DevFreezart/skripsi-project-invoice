package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.dto.ProjectProgressReport;
import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectProgressReportService {

    private final ProjectRepository projectRepository;

    public JasperPrint printReport(LocalDate startDate, LocalDate endDatee) throws Exception {

        List<ProjectProgressReport> invoiceReports = buildDataReport(startDate,endDatee);

        JRDataSource ds = new JRBeanCollectionDataSource(invoiceReports);

        Resource report = new ClassPathResource("templates/report/project_progress_report.jrxml");
        Resource logoSrc = new ClassPathResource("static/img/logo.png");

        Map<String, Object> parameters = new HashMap<>();
        BufferedImage logo = ImageIO.read(logoSrc.getURL());
        Locale locale = new Locale("id", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", locale);
        String start = ObjectUtils.isEmpty(startDate) ? "":formatter.format(startDate);
        String end = ObjectUtils.isEmpty(endDatee) ? "":formatter.format(endDatee);

        parameters.put("tglStart", start);
        parameters.put("tglEnd", end);
        parameters.put("logo",logo);

        JasperReport jasperReport = JasperCompileManager.compileReport(report.getInputStream());

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);

        return jasperPrint;

    }

    private List<ProjectProgressReport> buildDataReport(LocalDate startDate, LocalDate endDatee) {

        List<Project> invoiceList = new ArrayList<>();

        if (startDate ==  null && endDatee == null) {
            invoiceList = projectRepository.findAll();
        } else if (endDatee == null) {
            invoiceList = projectRepository.findByStartDateGreaterThanEqual(startDate);
        } else if (startDate == null) {
            invoiceList = projectRepository.findByStartDateLessThanEqual(endDatee);
        } else {

            invoiceList = projectRepository.findByStartDateGreaterThanEqualAndStartDateLessThanEqual(startDate,endDatee);
        }

        List<ProjectProgressReport> report = invoiceList.stream()
                .map(
                        project -> {
                            return ProjectProgressReport.builder()
                                    .noProject(project.getNoProject())
                                    .title(project.getTitle())
                                    .client(project.getClient())
                                    .location(project.getLocation())
                                    .pic(project.getPic())
                                    .nospk(project.getNospk())
                                    .startDate(project.getStartDate())
                                    .endDate(project.getEndDate())
                                    .status(project.getStatus())
                                    .build();
                        }
                ).collect(Collectors.toList());

        if (report.isEmpty()) {
            report = Arrays.asList(ProjectProgressReport.builder().build());
        }

        return report;

    }

}
