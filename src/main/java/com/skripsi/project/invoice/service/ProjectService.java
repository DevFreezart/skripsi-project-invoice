package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public Page<Project> findAllByNoProject(String noProject, Pageable pageable, LocalDate startDate, LocalDate endDatee) {


        if (startDate ==  null && endDatee == null) {
            return projectRepository.findAllByNoProjectLike("%"+noProject+"%",pageable);
        } else if (endDatee == null) {
            return projectRepository.findByStartDateGreaterThanEqualAndNoProjectLike(startDate, "%"+noProject+"%",pageable);
        } else if (startDate == null) {
            return projectRepository.findByStartDateLessThanEqualAndNoProjectLike(endDatee, "%"+noProject+"%",pageable);
        } else {
            return projectRepository.findByStartDateGreaterThanEqualAndStartDateLessThanEqualAndNoProjectLike(startDate,endDatee, "%"+noProject+"%",pageable);
        }
    }

    public Project findById(Long id) {
        Optional<Project> checkProject =  projectRepository.findById(id);
        if (checkProject.isPresent()) {
            return checkProject.get();
        }

        return null;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project save(Project project) {
        return projectRepository.save(project);
    }

    public void deleteById(Long id) {
        projectRepository.deleteById(id);
    }

    public Long getLastId() {
        return projectRepository.lastIdProject();
    }

}
