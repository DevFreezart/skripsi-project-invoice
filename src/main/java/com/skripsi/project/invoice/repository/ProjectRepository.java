package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project,Long>, JpaSpecificationExecutor<Project> {

    Page<Project> findAllByNoProjectLike(String noProject, Pageable pageable);

    List<Project> findByStartDateGreaterThanEqualAndStartDateLessThanEqual(LocalDate start, LocalDate end);
    List<Project> findByStartDateGreaterThanEqual(LocalDate start);
    List<Project> findByStartDateLessThanEqual(LocalDate end);

    Page<Project> findByStartDateGreaterThanEqualAndStartDateLessThanEqualAndNoProjectLike(LocalDate start, LocalDate end,String noProject, Pageable pageable);
    Page<Project> findByStartDateGreaterThanEqualAndNoProjectLike(LocalDate start,String noProject, Pageable pageable);
    Page<Project> findByStartDateLessThanEqualAndNoProjectLike(LocalDate end,String noProject, Pageable pageable);

    @Query(value = "select max(id) from project ", nativeQuery = true)
    Long lastIdProject();

}

