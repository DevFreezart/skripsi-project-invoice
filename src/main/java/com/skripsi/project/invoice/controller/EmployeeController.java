package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Employee;
import com.skripsi.project.invoice.entity.Position;
import com.skripsi.project.invoice.form.EmployeeUpdateForm;
import com.skripsi.project.invoice.form.EmpolyeeAddForm;
import com.skripsi.project.invoice.service.EmployeeService;
import com.skripsi.project.invoice.service.PositionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class EmployeeController {

    private final PositionService positionService;
    
    private final EmployeeService employeeService;

    private final ModelMapper modelMapper;

    @GetMapping("/employee/list")
    public ModelAndView employeeListGet() {
        ModelAndView modelAndView = new ModelAndView("employee");

        return modelAndView;
    }

    @GetMapping("/employee/add")
    public ModelAndView employeeAddGet() {
        ModelAndView modelAndView = new ModelAndView("employee-add");

        List<Position> positionList = positionService.findAll();

        modelAndView.addObject("employeeAddForm", new EmpolyeeAddForm());
        modelAndView.addObject("positionList",positionList);

        return modelAndView;
    }

    @PostMapping("/employee/add")
    public ModelAndView employeeAddPost(@Valid EmpolyeeAddForm request, BindingResult result, HttpServletRequest httpServletRequest) {
        log.debug("employee request: {}",  request);
        ModelAndView modelAndView = new ModelAndView("redirect:/employee/list");

        modelAndView.setViewName("employee-add");

        Employee employee = modelMapper.map(request,Employee.class);
        Position position = new Position();
        position.setId(request.getPositionId());
        employee.setPosition(position);
        employee.setId(null);

        employeeService.save(employee);

        modelAndView.addObject("employeeAddForm", new EmpolyeeAddForm());

        return modelAndView;
    }

    @GetMapping("/employee/update/{id}")
    public ModelAndView employeeUpdateGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("employee-update");

        Employee employee = employeeService.findById(id);

        if (employee == null){
            modelAndView.setViewName("redirect:/employee/list");
            return modelAndView;
        }

        EmployeeUpdateForm employeeUpdateForm = modelMapper.map(employee,EmployeeUpdateForm.class);
        employeeUpdateForm.setPositionId(employee.getPosition().getId());
        log.debug("updateEmployee: {}", employeeUpdateForm);

        List<Position> positionList = positionService.findAll();

        modelAndView.addObject("employeeUpdateForm", employeeUpdateForm);
        modelAndView.addObject("positionList",positionList);

        return modelAndView;
    }

    @PostMapping("/employee/update/{id}")
    public ModelAndView employeeUpdatePost(@PathVariable("id") Long id, @Valid EmployeeUpdateForm employeeUpdateForm, BindingResult result, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView("redirect:/employee/list");

        if (!result.hasErrors()){
            log.debug(employeeUpdateForm.toString());
            Employee employee = modelMapper.map(employeeUpdateForm,Employee.class);
            Position position = new Position();
            position.setId(employeeUpdateForm.getPositionId());
            employee.setPosition(position);
            employeeService.save(employee);
        }else {
            modelAndView.setViewName("employee-update");

            modelAndView.addObject("employeeUpdateForm", employeeUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/employee/delete/{id}")
    public ModelAndView employeeAddGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("redirect:/employee/list");
        
        employeeService.deleteById(id);

        return modelAndView;
    }
}
