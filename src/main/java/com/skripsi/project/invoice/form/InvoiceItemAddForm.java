package com.skripsi.project.invoice.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceItemAddForm {

    private Long idInvoice;

    private String deskripsi;

    private String satuan;

    private Integer volume;

    private BigDecimal hargasatuan;

    private BigDecimal hargajumlah;

}
