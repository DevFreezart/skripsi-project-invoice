package com.skripsi.project.invoice.controller.api;

import com.skripsi.project.invoice.domain.pagination.DataTableRequest;
import com.skripsi.project.invoice.domain.pagination.DataTableResults;
import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.service.ProjectService;
import com.skripsi.project.invoice.utils.AppUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class ProjectAPIController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/project/list", method = RequestMethod.GET, params = {"datatables"}, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataTableResults historyListDatatables(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        DataTableRequest<Project> dataTableRequest = new DataTableRequest(httpServletRequest);

        int page = dataTableRequest.getPage();
        int limit = dataTableRequest.getLength();

        String start = AppUtil.getParameter(httpServletRequest,"startDate", "");
        String end = AppUtil.getParameter(httpServletRequest,"endDate", "");
        log.debug("startDate: {}, endDate: {}", start,end);
        LocalDate startDate = ObjectUtils.isEmpty(start) ? null:LocalDate.parse(start);
        LocalDate endDate = ObjectUtils.isEmpty(end) ? null:LocalDate.parse(end);

        String search = (dataTableRequest.getSearch() == null ? "" : dataTableRequest.getSearch());

        String sortDirection = dataTableRequest.getOrder().getSortDir();

        Pageable pageable = null;

        if (sortDirection.equalsIgnoreCase("DESC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "noProject"));
        } else if (sortDirection.equalsIgnoreCase("ASC")) {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "noProject"));
        } else {
            pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "noProject"));

        }

        Page<Project> pageProject;
        pageProject = projectService.findAllByNoProject(search, pageable,startDate,endDate);

        List<Project> projectList = pageProject.getContent();

        DataTableResults<Project> dataTableResults = new DataTableResults<Project>();

        dataTableResults.setListOfDataObjects(projectList);
        dataTableResults.setDraw(dataTableRequest.getDraw());
        if (!projectList.isEmpty()) {
            dataTableResults.setRecordsTotal(String.valueOf(pageProject.getTotalElements()));
            dataTableResults.setRecordsFiltered(String.valueOf(pageProject.getTotalElements()));
        }

        return dataTableResults;
    }
}
