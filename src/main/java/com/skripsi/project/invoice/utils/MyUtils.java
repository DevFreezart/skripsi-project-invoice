package com.skripsi.project.invoice.utils;

/**
 * @author isman
 * @Project myindo-platform-security
 * @Package id.myindo.platform.security.utils
 * @ON 2/10/19
 */

public class MyUtils {

    private static final String ALPHA_STRING = "ABCDEFGHJKMNPQRSTUVWXYZ";
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz123456789";
    private static final String ALPHA_NUMERIC = "0123456789";
    public static final String USER_DETAIL = "USER:DETAIL";
    public static final String USER_CONFIG = "USER:CONFIG";
    public static final String USER_IPADDRESS = "USER:IPADDRESS";
    public static final String USER_ATTRIBUTE = "USER:ATTRIBUTE";

    public String generateRandom(int count) {
        StringBuilder builder = new StringBuilder();
        int characterFirst = (int)(Math.random()*ALPHA_STRING.length());
        builder.append(ALPHA_NUMERIC_STRING.charAt(characterFirst));
        count--;

        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public String generateNumericRandom(int count) {
        StringBuilder builder = new StringBuilder();
        int characterFirst = (int)(Math.random()*ALPHA_NUMERIC.length());
        builder.append(ALPHA_NUMERIC.charAt(characterFirst));
        count--;

        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC.length());
            builder.append(ALPHA_NUMERIC.charAt(character));
        }
        return builder.toString();
    }

    public String memoryUserKey(String prefix, String key) {
        return memoryUserKey(prefix,key,"");
    }

    public String memoryUserKey(String prefix, String key, String suffix) {
        String shaKey = "";
        if (suffix.trim().isEmpty()) {
            shaKey = "ACCOUNT:" + prefix.toUpperCase() + ":" +  key.toUpperCase();
        } else {
            shaKey = "USER:" +  prefix.toUpperCase() + ":" +  key.toUpperCase() + ":" + suffix.toUpperCase();
        }

        return shaKey;
    }

    /**
     * @param value
     * @return Boolean
     * @description Menjumlah urutan ganjil dan genap untuk validasi Luhn
     */
    public Boolean checkLuhn(String value) {

        Integer[] ccNum = stringToNumArray(value);

        StringBuilder builderGenap = new StringBuilder();
        StringBuilder builderGanjil = new StringBuilder();

        for (int i = ccNum.length - 1; i >= 0; i--) {
            if ((i % 2) == 0) {
                Integer total = ccNum[i] * 2;

                if (total > 9) {
                    builderGenap.append(sumTwoDigit(total.toString()));
                } else {
                    builderGenap.append(total);
                }
            } else {
                builderGanjil.append(ccNum[i]);
            }
        }

        Integer[] genap = stringToNumArray(builderGenap.toString());
        Integer[] ganjil = stringToNumArray(builderGanjil.toString());

        return validation(genap, ganjil);
    }

    /**
     * @param number
     * @return Integer Array
     * @description Mengubah String menjadi Integer Array
     */
    private Integer[] stringToNumArray(String number) {
        Integer[] numbers = new Integer[number.length()];

        for (int i = 0; i < number.length(); i++) {
            numbers[i] = Integer.parseInt(String.valueOf(number.charAt(i)));
        }

        return numbers;
    }

    /**
     * @param number
     * @return Integer
     * @description Menjumlah angka yang terdiri dari 2 digit
     */
    public Integer sumTwoDigit(String number) {
        Integer[] numbers = new Integer[number.length()];

        Integer sum = 0;

        for (int i = 0; i < number.length(); i++) {
            numbers[i] = Integer.parseInt(String.valueOf(number.charAt(i)));
            sum = sum + numbers[i];
        }

        return sum;
    }

    /**
     * @param ganjil
     * @param genap
     * @return Boolean
     * @description Menjumlah urutan ganjil dan genap untuk validasi Luhn
     */
    private Boolean validation(Integer[] ganjil, Integer[] genap) {
        Integer totalGenap = 0;
        Integer totalGanjil = 0;

        for (int i = 0; i <= ganjil.length - 1; i++) {
            totalGanjil = ganjil[i] + totalGanjil;
        }

        for (int i = 0; i <= genap.length - 1; i++) {
            totalGenap = genap[i] + totalGenap;
        }

        int total = totalGanjil + totalGenap;

        if ((total % 10) == 0)
            return true;

        return false;
    }

}
