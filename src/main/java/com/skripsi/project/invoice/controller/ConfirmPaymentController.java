package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.ConfirmPayment;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.ConfirmPayment;
import com.skripsi.project.invoice.form.ConfirmPaymentAddForm;
import com.skripsi.project.invoice.service.ConfirmPaymentService;
import com.skripsi.project.invoice.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ConfirmPaymentController {

    private final ConfirmPaymentService confirmPaymentService;

    private final InvoiceService invoiceService;

    private final ModelMapper modelMapper;

    @GetMapping("/confirm-payment/list")
    public ModelAndView confirmPaymentListGet() {
        ModelAndView modelAndView = new ModelAndView("confirmPayment");

        return modelAndView;
    }

    @GetMapping("/confirm-payment/invoice/{id}")
    public ModelAndView confirmPaymentAddGet(@PathVariable("id") Long invoiceId) {
        ModelAndView modelAndView = new ModelAndView("confirm-payment-add");

        Invoice invoice = invoiceService.findById(invoiceId);

        ConfirmPaymentAddForm confirmPaymentAddForm = new ConfirmPaymentAddForm();
        confirmPaymentAddForm.setAmount(invoice.getGrandtotal());
        confirmPaymentAddForm.setIdInvoice(invoice.getId());
        confirmPaymentAddForm.setInvoiceNo(invoice.getNoInvoice());

        ConfirmPayment confirmPayment = confirmPaymentService.findByInvoiceId(invoiceId);
        if (confirmPayment != null) {
            confirmPaymentAddForm.setId(confirmPayment.getId());
            confirmPaymentAddForm.setPaymentDate(confirmPayment.getPaymentDate());
            confirmPaymentAddForm.setJournal(confirmPayment.getJournal());
            confirmPaymentAddForm.setReceipent(confirmPayment.getReceipent());
            confirmPaymentAddForm.setLinkStruk(confirmPayment.getLinkStruk());
            modelAndView.addObject("strukFileName", confirmPayment.getStrukFileName());
        }

        modelAndView.addObject("invoice", invoiceService.findById(invoiceId));
        modelAndView.addObject("confirmPaymentAddForm", confirmPaymentAddForm);
        modelAndView.addObject("journals", buildJournal());
        modelAndView.addObject("invoiceStatus", invoice.getStatus());

        return modelAndView;
    }

    @PostMapping("/confirm-payment/invoice/{id}")
    public ModelAndView confirmPaymentAddPost(@Valid ConfirmPaymentAddForm request, BindingResult result, @PathVariable("id") Long invoiceId, HttpServletRequest httpServletRequest) throws Exception {
        ModelAndView modelAndView = new ModelAndView("redirect:/invoice/list");

        ConfirmPayment confirmPayment = modelMapper.map(request,ConfirmPayment.class);
        confirmPayment.setId(null);
        if (request.getIdInvoice() != null) {
            confirmPayment.setInvoice(invoiceService.findById(request.getIdInvoice()));
        }

        if (request.getStrukFile() != null) {
            confirmPayment.setStrukFileName(request.getStrukFile().getOriginalFilename());
            confirmPayment.setStrukFileSize(request.getStrukFile().getSize());
            confirmPayment.setStrukFileContent(request.getStrukFile().getBytes());
        }

        confirmPaymentService.save(confirmPayment);

        return modelAndView;
    }

    private List<String> buildJournal() {
        return Arrays.asList(
                "CASH",
                "BCA"
        );
    }

    @GetMapping("/confirm-payment/downloadfile")
    public void downloadFile(@Param("id") Long id, Model model, HttpServletResponse response) throws IOException {
        ConfirmPayment confirmPayment = confirmPaymentService.findById(id);

        String fileName = confirmPayment.getStrukFileName();
        byte[] content = confirmPayment.getStrukFileContent();

        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename = " + fileName;
        response.setHeader(headerKey, headerValue);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(content);
        outputStream.close();

    }
}
