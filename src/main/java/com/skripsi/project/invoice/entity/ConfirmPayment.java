package com.skripsi.project.invoice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "confirm_payment")
public class ConfirmPayment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_invoice")
    private Invoice invoice;

    @Column(name = "journal", length = 15)
    private String journal;

    @Column(name = "receipent", length = 30)
    private String receipent;

    @Column(name = "bukti_transfer")
    private String linkStruk;

    @Column(name = "paymentdate")
    private LocalDate paymentDate;

    @Column(name = "struk_file_name")
    private String strukFileName;

    @Column(name = "struk_file_size")
    private Long strukFileSize;

    @Lob
    @Column(name = "struk_file_content")
    private byte [] strukFileContent;

}
