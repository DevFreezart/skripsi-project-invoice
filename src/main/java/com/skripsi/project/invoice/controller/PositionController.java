package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Position;
import com.skripsi.project.invoice.form.PositionAddForm;
import com.skripsi.project.invoice.form.PositionUpdateForm;
import com.skripsi.project.invoice.service.PositionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequiredArgsConstructor
public class PositionController {
    
    private final PositionService positionService;

    private final ModelMapper modelMapper;

    @GetMapping("/position/list")
    public ModelAndView positionListGet() {
        ModelAndView modelAndView = new ModelAndView("position");

        return modelAndView;
    }

    @GetMapping("/position/add")
    public ModelAndView positionAddGet() {
        ModelAndView modelAndView = new ModelAndView("position-add");

        modelAndView.addObject("positionAddForm", new PositionAddForm());

        return modelAndView;
    }

    @PostMapping("/position/add")
    public ModelAndView positionAddPost(@Valid PositionAddForm request, BindingResult result, HttpServletRequest httpServletRequest) {
        ModelAndView modelAndView = new ModelAndView("redirect:/position/list");

        modelAndView.setViewName("position-add");

        Position position = modelMapper.map(request,Position.class);
        position.setId(null);

        positionService.save(position);

        modelAndView.addObject("positionAddForm", new PositionAddForm());

        return modelAndView;
    }

    @GetMapping("/position/update/{id}")
    public ModelAndView positionUpdateGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("position-update");

        Position position = positionService.findById(id);

        if (position == null){
            modelAndView.setViewName("redirect:/position/list");
            return modelAndView;
        }

        PositionUpdateForm positionUpdateForm = modelMapper.map(position,PositionUpdateForm.class);

        modelAndView.addObject("positionUpdateForm", positionUpdateForm);

        return modelAndView;
    }

    @PostMapping("/position/update/{id}")
    public ModelAndView positionUpdatePost(@PathVariable("id") Long id, @Valid PositionUpdateForm positionUpdateForm, BindingResult result, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView("redirect:/position/list");

        if (!result.hasErrors()){
            log.debug(positionUpdateForm.toString());
            Position position = modelMapper.map(positionUpdateForm,Position.class);
            positionService.save(position);
        }else {
            modelAndView.setViewName("position-update");

            modelAndView.addObject("positionUpdateForm", positionUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/position/delete/{id}")
    public ModelAndView positionAddGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("redirect:/position/list");
        
        positionService.deleteById(id);

        return modelAndView;
    }
}
