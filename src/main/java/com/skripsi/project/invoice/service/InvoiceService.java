package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.repository.InvoiceItemRepository;
import com.skripsi.project.invoice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;

    private final InvoiceItemRepository invoiceItemRepository;


    public Page<Invoice> findAllByNo(String no, Pageable pageable, LocalDate startDate, LocalDate endDatee) {

        if (startDate ==  null && endDatee == null) {
            return invoiceRepository.findAllByNoInvoiceLike("%"+no+"%",pageable);
        } else if (endDatee == null) {
            return invoiceRepository.findByInvoiceDateGreaterThanEqualAndNoInvoiceLike(startDate, "%"+no+"%",pageable);
        } else if (startDate == null) {
            return invoiceRepository.findByInvoiceDateLessThanEqualAndNoInvoiceLike(endDatee, "%"+no+"%",pageable);
        } else {
            return invoiceRepository.findByInvoiceDateGreaterThanEqualAndInvoiceDateLessThanEqualAndNoInvoiceLike(startDate,endDatee, "%"+no+"%",pageable);
        }

    }

    public Invoice findById(Long id) {
        Optional<Invoice> checkInvoice =  invoiceRepository.findById(id);
        if (checkInvoice.isPresent()) {
            return checkInvoice.get();
        }

        return null;
    }

    public Invoice findByNo(String no) {
        Optional<Invoice> checkInvoice =  invoiceRepository.findFirstByNoInvoice(no);
        if (checkInvoice.isPresent()) {
            return checkInvoice.get();
        }

        return null;
    }

    public Invoice save(Invoice invoice) {

        BigDecimal percentase = invoice.getPercent().divide(BigDecimal.valueOf(100));
        BigDecimal harga = invoice.getProject().getHarga().multiply(percentase).setScale(0,RoundingMode.CEILING);
        invoice.setHarga(harga);
        if (invoice.getId() != null) {
            BigDecimal subTotal = invoiceItemRepository.getTotalHarga(invoice.getId());
            BigDecimal ppn = invoice.getPpn() == null ? BigDecimal.ZERO : invoice.getPpn();
            invoice.setSubtotal(subTotal == null ?BigDecimal.ZERO:subTotal);
            BigDecimal ppnAmount = (ppn.divide(BigDecimal.valueOf(100),12, RoundingMode.HALF_UP)).multiply(invoice.getSubtotal());
            invoice.setPpnAmount(ppnAmount);
            invoice.setGrandtotal(invoice.getSubtotal().add(ppnAmount));
        }

        return invoiceRepository.save(invoice);
    }

    public void deleteById(Long id) {
        invoiceRepository.deleteById(id);
    }

}
