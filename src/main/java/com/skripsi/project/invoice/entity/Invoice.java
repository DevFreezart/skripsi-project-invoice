package com.skripsi.project.invoice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "invoice")
public class Invoice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_project")
    private Project project;

    @Column(name = "no_invoice", length = 20)
    private String noInvoice;

    @Column(name = "skema", length = 10)
    private String skema;

    @Column(name = "percent")
    private BigDecimal percent;;

    @Column(name = "status", length = 10)
    private String status;

    @Column(name = "address")
    private String address;

    @Column(name = "invoice_date")
    private LocalDate invoiceDate;

    @Column(name = "ppn")
    private BigDecimal ppn;

    @Column(name = "ppn_amount")
    private BigDecimal ppnAmount;

    @Column(name = "subtotal")
    private BigDecimal subtotal;

    @Column(name = "grandtotal")
    private BigDecimal grandtotal;

    @Column(name = "harga")
    private BigDecimal harga;

}
