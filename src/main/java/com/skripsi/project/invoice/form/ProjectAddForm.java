package com.skripsi.project.invoice.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectAddForm {

    private String noProject;

    private String title;

    private String client;

    private String location;

    private String pic;

    private String nospk;

    private String spk;

    private MultipartFile spkFile;

    private BigDecimal harga;

}
