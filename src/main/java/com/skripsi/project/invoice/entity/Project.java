package com.skripsi.project.invoice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "project")
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_employyee")
    private Employee employee;

    @Column(name = "no_project", length = 10)
    private String noProject;

    @Column(name = "title", length = 100)
    private String title;

    @Column(name = "client", length = 50)
    private String client;

    @Column(name = "location")
    private String location;

    @Column(name = "pic", length = 20)
    private String pic;

    @Column(name = "nospk", length = 50)
    private String nospk;

    @Column(name = "spk")
    private String spk;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "doc_project")
    private String docProject;

    @Column(name = "bap")
    private String bap;

    @Column(name = "status", length = 10)
    private String status;

    @Column(name = "harga")
    private BigDecimal harga;

    @Column(name = "doc_name")
    private String docName;

    @Column(name = "doc_size")
    private Long docSize;

    @Lob
    @Column(name = "doc_content")
    private byte [] docContent;

    @Column(name = "bap_name")
    private String bapName;

    @Column(name = "bap_size")
    private Long bapSize;

    @Lob
    @Column(name = "bap_content")
    private byte [] bapContent;

    @Column(name = "spk_name")
    private String spkName;

    @Column(name = "spk_size")
    private Long spkSize;

    @Lob
    @Column(name = "spk_content")
    private byte [] spkContent;

}
