package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.form.ProjectAddForm;
import com.skripsi.project.invoice.form.ProjectProgressUpdateForm;
import com.skripsi.project.invoice.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ProjectProgressController {

    private final ProjectService projectService;

    private final ModelMapper modelMapper;

    @GetMapping("/project/progress/list")
    public ModelAndView projectListGet() {
        ModelAndView modelAndView = new ModelAndView("project-progress");

        return modelAndView;
    }

    @GetMapping("/project/progress/update/{id}")
    public ModelAndView projectProgressUpdateGet(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("project-progress-update");

        Project project = projectService.findById(id);

        if (project == null) {
            modelAndView.setViewName("redirect:/project/progress/list");
            return modelAndView;
        }

        ProjectProgressUpdateForm projectProgressUpdateForm = modelMapper.map(project, ProjectProgressUpdateForm.class);

        modelAndView.addObject("projectProgressUpdateForm", projectProgressUpdateForm);
        modelAndView.addObject("projectStatus", buildProjectStatus());
        modelAndView.addObject("docName", project.getDocName());
        modelAndView.addObject("bapName", project.getBapName());

        return modelAndView;
    }

    @PostMapping("/project/progress/update/{id}")
    public ModelAndView projectProgressUpdatePost(@PathVariable("id") Long id, @Valid ProjectProgressUpdateForm projectProgressUpdateForm, BindingResult result, HttpServletRequest httpServletRequest) throws Exception {
        ModelAndView modelAndView = new ModelAndView("redirect:/project/progress/list");

        if (!result.hasErrors()) {
            log.debug(projectProgressUpdateForm.toString());
            Project project = projectService.findById(id);
            BeanUtils.copyProperties(projectProgressUpdateForm, project);
            if (projectProgressUpdateForm.getDocProjectFile() != null) {
                project.setDocName(projectProgressUpdateForm.getDocProjectFile().getOriginalFilename());
                project.setDocProject(projectProgressUpdateForm.getDocProjectFile().getOriginalFilename());
                project.setDocSize(projectProgressUpdateForm.getDocProjectFile().getSize());
                project.setDocContent(projectProgressUpdateForm.getDocProjectFile().getBytes());
            }
            if (projectProgressUpdateForm.getBap() != null) {
                project.setBap(projectProgressUpdateForm.getBapFile().getOriginalFilename());
                project.setBapName(projectProgressUpdateForm.getBapFile().getOriginalFilename());
                project.setBapSize(projectProgressUpdateForm.getBapFile().getSize());
                project.setBapContent(projectProgressUpdateForm.getBapFile().getBytes());
            }
            projectService.save(project);
        } else {
            modelAndView.setViewName("project-progress-update");

            modelAndView.addObject("projectProgressUpdateForm", projectProgressUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/project/progress/delete/{id}")
    public ModelAndView projectAddGet(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/project/progress/list");

        projectService.deleteById(id);

        return modelAndView;
    }


    private List<String> buildProjectStatus() {
        return Arrays.asList(
                "PROGRESS",
                "PENDING",
                "FINISH"
        );
    }

    @GetMapping("/downloadfile")
    public void downloadFile(@Param("id") Long id, @Param("name") String name, Model model, HttpServletResponse response) throws IOException {
        Project project = projectService.findById(id);

        String fileName = "";
        byte[] content = null;
        if (name.equalsIgnoreCase("DOC")) {
            fileName = project.getDocName();
            content = project.getDocContent();
        } else if (name.equalsIgnoreCase("BAP")){
            fileName = project.getBapName();
            content = project.getBapContent();
        } else {
            fileName = project.getSpkName();
            content = project.getSpkContent();
        }
        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename = " + fileName;
        response.setHeader(headerKey, headerValue);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(content);
        outputStream.close();

    }
}
