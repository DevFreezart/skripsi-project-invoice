package com.skripsi.project.invoice.controller;

import com.skripsi.project.invoice.entity.Project;
import com.skripsi.project.invoice.form.ProjectAddForm;
import com.skripsi.project.invoice.form.ProjectUpdateForm;
import com.skripsi.project.invoice.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    private final ModelMapper modelMapper;

    @GetMapping("/project/list")
    public ModelAndView projectListGet() {
        ModelAndView modelAndView = new ModelAndView("project");

        return modelAndView;
    }

    @GetMapping("/project/add")
    public ModelAndView projectAddGet() {
        ModelAndView modelAndView = new ModelAndView("project-add");
        String year = new SimpleDateFormat("yy").format(new Date());
        Long lastId = projectService.getLastId();
        if (lastId == null) {
            lastId = 1L;
        } else {
            lastId ++;
        }
        String projectNo = "P" + fillNum(lastId) + "" + year;
        ProjectAddForm form = new ProjectAddForm();
        form.setNoProject(projectNo);
        modelAndView.addObject("projectAddForm", form);

        return modelAndView;
    }

    @PostMapping("/project/add")
    public ModelAndView projectAddPost(@Valid ProjectAddForm request, BindingResult result, HttpServletRequest httpServletRequest) throws Exception {
        ModelAndView modelAndView = new ModelAndView("redirect:/project/list");

        Project project = modelMapper.map(request,Project.class);
        project.setId(null);
        log.info("spkFile: {}",request);
        if (request.getSpkFile() != null) {
            log.debug("spk");
            project.setSpkName(request.getSpkFile().getOriginalFilename());
            project.setSpkSize(request.getSpkFile().getSize());
            project.setSpkContent(request.getSpkFile().getBytes());
        }

        projectService.save(project);

        return modelAndView;
    }

    @GetMapping("/project/update/{id}")
    public ModelAndView projectUpdateGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("project-update");

        Project project = projectService.findById(id);

        if (project == null){
            modelAndView.setViewName("redirect:/project/list");
            return modelAndView;
        }

        ProjectUpdateForm projectUpdateForm = modelMapper.map(project,ProjectUpdateForm.class);

        modelAndView.addObject("projectUpdateForm", projectUpdateForm);
        modelAndView.addObject("spkName", project.getSpkName());

        return modelAndView;
    }

    @PostMapping("/project/update/{id}")
    public ModelAndView projectUpdatePost(@PathVariable("id") Long id, @Valid ProjectUpdateForm projectUpdateForm, BindingResult result, HttpServletRequest httpServletRequest) throws Exception{
        ModelAndView modelAndView = new ModelAndView("redirect:/project/list");

        if (!result.hasErrors()){
            log.debug(projectUpdateForm.toString());
            Project project = modelMapper.map(projectUpdateForm,Project.class);

            if (projectUpdateForm.getSpkFile() != null) {
                project.setSpkName(projectUpdateForm.getSpkFile().getOriginalFilename());
                project.setSpkSize(projectUpdateForm.getSpkFile().getSize());
                project.setSpkContent(projectUpdateForm.getSpkFile().getBytes());
            }
            projectService.save(project);
        }else {
            modelAndView.setViewName("project-update");

            modelAndView.addObject("projectUpdateForm", projectUpdateForm);
            log.debug("Error validation");
            log.debug(result.getAllErrors().toString());
        }

        return modelAndView;
    }

    @GetMapping("/project/delete/{id}")
    public ModelAndView projectAddGet(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("redirect:/project/list");

        projectService.deleteById(id);

        return modelAndView;
    }

    private String fillNum(Long num) {
        String no = num+"";
        int length = 4;

        if (no.length() < length) {
            int lengtDivide = length - no.length();
            String prefix = "0";
            for (int i = 1; i < lengtDivide; i++) {
                prefix += "0";
            }
            return prefix + no;
        }
        return no;
    }
}
