package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice,Long>, JpaSpecificationExecutor<Invoice> {

    Page<Invoice> findAllByNoInvoiceLike(String noInvoice, Pageable pageable);

    Optional<Invoice> findFirstByNoInvoice(String no);

    List<Invoice> findByInvoiceDateGreaterThanEqualAndInvoiceDateLessThanEqual(LocalDate start, LocalDate end);


    List<Invoice> findByInvoiceDateGreaterThanEqual(LocalDate startDate);

    List<Invoice> findByInvoiceDateLessThanEqual(LocalDate endDatee);

    Page<Invoice> findByInvoiceDateGreaterThanEqualAndInvoiceDateLessThanEqualAndNoInvoiceLike(LocalDate start, LocalDate end, String noInvoice,Pageable pageable);


    Page<Invoice> findByInvoiceDateGreaterThanEqualAndNoInvoiceLike(LocalDate startDate, String noInvoice,Pageable pageable);

    Page<Invoice> findByInvoiceDateLessThanEqualAndNoInvoiceLike(LocalDate endDatee, String noInvoice,Pageable pageable);
}
