package com.skripsi.project.invoice.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmPaymentAddForm {

    private Long id;

    private Long idInvoice;

    private String invoiceNo;

    private String journal;

    private String linkStruk;

    private String receipent;

    private BigDecimal amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate paymentDate;

    private MultipartFile strukFile;

}
