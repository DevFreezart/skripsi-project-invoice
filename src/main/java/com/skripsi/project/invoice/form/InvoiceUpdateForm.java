package com.skripsi.project.invoice.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceUpdateForm {

    private Long id;

    private Long idProject;

    private String noInvoice;

    private String skema;

    private String status;

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate invoiceDate;

    private BigDecimal ppn;

    private BigDecimal subtotal;

    private BigDecimal grandtotal;

    private BigDecimal percent;

    private BigDecimal harga;

}
