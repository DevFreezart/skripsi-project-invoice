package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Employee;
import com.skripsi.project.invoice.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final PasswordEncoder passwordEncoder;

    public Page<Employee> findAllByNip(String nip, Pageable pageable) {
        return employeeRepository.findAllByNipLike("%"+nip+"%",pageable);
    }

    public Employee findById(Long id) {
        Optional<Employee> checkEmployee =  employeeRepository.findById(id);
        if (checkEmployee.isPresent()) {
            return checkEmployee.get();
        }

        return null;
    }

    public Employee findByNip(String nip) {
        Optional<Employee> checkEmployee =  employeeRepository.findFirstByNip(nip);
        if (checkEmployee.isPresent()) {
            return checkEmployee.get();
        }

        return null;
    }

    public Employee save(Employee employee) {
        employee.setPassword(passwordEncoder.encode(employee.getPassword()));
        return employeeRepository.save(employee);
    }

    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }

}
