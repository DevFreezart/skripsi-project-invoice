package com.skripsi.project.invoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkripsiProjectInvoiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkripsiProjectInvoiceApplication.class, args);
    }

}
