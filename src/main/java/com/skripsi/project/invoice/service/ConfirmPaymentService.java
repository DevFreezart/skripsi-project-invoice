package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.ConfirmPayment;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.repository.ConfirmPaymentRepository;
import com.skripsi.project.invoice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmPaymentService {

    private final ConfirmPaymentRepository confirmRepository;

    private final InvoiceRepository invoiceRepository;


    public ConfirmPayment findById(Long id) {
        Optional<ConfirmPayment> checkConfirmPayment =  confirmRepository.findById(id);
        if (checkConfirmPayment.isPresent()) {
            return checkConfirmPayment.get();
        }

        return null;
    }


    public ConfirmPayment findByInvoiceId(Long id) {
        Optional<ConfirmPayment> checkConfirmPayment =  confirmRepository.findByInvoiceId(id);
        if (checkConfirmPayment.isPresent()) {
            return checkConfirmPayment.get();
        }

        return null;
    }

    @Transactional
    public ConfirmPayment save(ConfirmPayment confirm) {
        Invoice invoice = confirm.getInvoice();
        invoice.setStatus("PAID");
        invoiceRepository.save(invoice);
        confirmRepository.deleteAllByInvoiceId(invoice.getId());
        return confirmRepository.save(confirm);
    }

    public void deleteById(Long id) {
        confirmRepository.deleteById(id);
    }

}
