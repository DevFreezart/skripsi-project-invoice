package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.entity.Employee;
import com.skripsi.project.invoice.repository.EmployeeRepository;
import com.skripsi.project.invoice.utils.WebContextHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

@Slf4j
@Service("customUserDetailsService")
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Employee> checkEmployee = employeeRepository.findFirstByNip(s);
        if (checkEmployee.isPresent()) {
            Employee employee = checkEmployee.get();
            if (employee != null) {
                return buildUserForAuthentication(employee, buildUserAuthority(employee.getPosition().getName()));
            } else {
                WebContextHolder webContextHolder = new WebContextHolder();
                HttpSession session = webContextHolder.getSession();

                session.setAttribute("loginErrorMessage", "Wrong NIP/password");
            }
        }

        return null;
    }

    private User buildUserForAuthentication(Employee employee, List<GrantedAuthority> grantedAuthorities) {
        return new User(employee.getName(), employee.getPassword(), true, true, true, true, buildUserAuthority(employee.getPosition().getName()));
    }

    private List<GrantedAuthority> buildUserAuthority(String role) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+role));
        return new ArrayList<>(grantedAuthorities);
    }

}
