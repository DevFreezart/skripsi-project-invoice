package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.InvoiceItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceItemRepository extends JpaRepository<InvoiceItem,Long>, JpaSpecificationExecutor<InvoiceItem> {

    List<InvoiceItem> findAllByInvoiceId(Long invoiceId);

    @Query(
            value = "select sum(subtotal) from invoice_item where id_invoice =:idInvoice",
            nativeQuery = true
    )
    BigDecimal getTotalHarga(@Param("idInvoice") Long invoiceId);

    @Query(
            value = "select sum(subtotal) from invoice_item where id_invoice =:idInvoice and id!=:id",
            nativeQuery = true
    )
    BigDecimal getTotalHargaWitoutItemId(@Param("idInvoice") Long invoiceId, @Param("id") Long id);

}
