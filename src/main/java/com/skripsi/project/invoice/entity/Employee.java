package com.skripsi.project.invoice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_position")
    private Position position;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "nip", length = 12)
    private String nip;

    @Column(name = "password")
    private String password;

}
