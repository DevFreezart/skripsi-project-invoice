package com.skripsi.project.invoice.service;

import com.skripsi.project.invoice.dto.InvoiceItemReport;
import com.skripsi.project.invoice.dto.InvoiceReport;
import com.skripsi.project.invoice.entity.Invoice;
import com.skripsi.project.invoice.entity.InvoiceItem;
import com.skripsi.project.invoice.repository.InvoiceItemRepository;
import com.skripsi.project.invoice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InvoiceReportService {

    private final InvoiceItemRepository invoiceItemRepository;

    private final InvoiceRepository invoiceRepository;

    private final ModelMapper modelMapper;

    public JasperPrint printReport(Long id) throws Exception {

        InvoiceReport invoiceReport = buildDataReport(id);

        List<InvoiceItemReport> invoiceItemReports = invoiceReport.getInvoiceItemReports();
        if (invoiceItemReports.isEmpty()) {
            invoiceItemReports = Arrays.asList(
                    InvoiceItemReport.builder()
                            .id(1L)
                            .deskripsi("")
                            .hargajumlah(BigDecimal.ZERO)
                            .hargasatuan(BigDecimal.ZERO)
                            .volume(0)
                            .satuan("")
                            .build()
            );
        }

        JRDataSource ds = new JRBeanCollectionDataSource(invoiceItemReports);

        Resource report = new ClassPathResource("templates/report/invoice.jrxml");
           /* Resource backgroundSrc = new ClassPathResource("jasper/background.jpeg");

            Resource logoOpacity50Src = new ClassPathResource("jasper/logoOpacity50.png");
            Resource signSrc = new ClassPathResource("jasper/sign.png");*/
        Resource logoSrc = new ClassPathResource("static/img/logo.png");

        Map<String, Object> parameters = new HashMap<>();
           /* BufferedImage background = ImageIO.read(backgroundSrc.getURL());
            BufferedImage logo = ImageIO.read(logoSrc.getURL());
            BufferedImage logoOpacity50 = ImageIO.read(logoOpacity50Src.getURL());
            BufferedImage sign = ImageIO.read(signSrc.getURL());*/
        BufferedImage logo = ImageIO.read(logoSrc.getURL());
        Locale locale = new Locale("id", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", locale);
        String date = formatter.format(invoiceReport.getInvoiceDate());

        parameters.put("address", invoiceReport.getAddress());
        parameters.put("noInvoice", invoiceReport.getNoInvoice());
        parameters.put("noPo", (invoiceReport.getProject() == null ? "-":invoiceReport.getProject().getNospk()));
        parameters.put("client", (invoiceReport.getProject() == null ? "-":invoiceReport.getProject().getClient()));
        parameters.put("projectName", (invoiceReport.getProject() == null ? "-":invoiceReport.getProject().getTitle()));
        parameters.put("total", invoiceReport.getSubtotal());
        parameters.put("grandTotal", invoiceReport.getGrandtotal());
        parameters.put("invoiceDate",date);
        parameters.put("ppn",invoiceReport.getPpnAmount() == null ? BigDecimal.ZERO:invoiceReport.getPpnAmount());
        parameters.put("logo",logo);

        JasperReport jasperReport = JasperCompileManager.compileReport(report.getInputStream());

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);

        return jasperPrint;

    }

    private InvoiceReport buildDataReport(Long id) {

        List<InvoiceItem> invoiceItemList = invoiceItemRepository.findAllByInvoiceId(id);

        if (!invoiceItemList.isEmpty()) {
            Map<Long, List<InvoiceItem>> dataMap = invoiceItemList.stream()
                    .collect(
                            Collectors.groupingBy(
                                    invoiceItem -> invoiceItem.getInvoice().getId(),
                                    Collectors.mapping(item -> item, Collectors.toList())
                            )
                    );

            if (dataMap.containsKey(id)) {
                List<InvoiceItem> invoiceItems = dataMap.get(id);
                InvoiceReport invoiceReport = modelMapper.map(invoiceItems.get(0).getInvoice(), InvoiceReport.class);
                invoiceReport.setInvoiceItemReports(
                        invoiceItems.stream()
                                .map(
                                        item -> modelMapper.map(item, InvoiceItemReport.class)
                                ).collect(Collectors.toList())
                );

                return invoiceReport;
            }

        } else {
            Optional<Invoice> checkInvoice = invoiceRepository.findById(id);
            if (checkInvoice.isPresent()) {
                return modelMapper.map(checkInvoice.get(), InvoiceReport.class);
            }
        }
        return null;
    }

}
