package com.skripsi.project.invoice.repository;

import com.skripsi.project.invoice.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long>, JpaSpecificationExecutor<Employee> {

    Page<Employee> findAllByNipLike(String noEmployee, Pageable pageable);

    Optional<Employee> findFirstByNip(String nip);

}
