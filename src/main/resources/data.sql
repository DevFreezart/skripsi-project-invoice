insert into `position`(`name`,title) values ('SA', 'Super Admin');
insert into `position`(`name`,title) values ('PELAKSANA', 'Pelaksana');
insert into `position`(`name`,title) values ('MANAGER_TEKNIK', 'Manager Teknik');
insert into `position`(`name`,title) values ('MANAGER_FINANCE', 'Manager KEUANGAN');
insert into employee(`name`,nip,id_position,password) values ('Super Admin','0',(select id from position where name='SA' limit 1),'$2a$10$To/okZ7RV.f5c2lNvUBHe.THDiLxaqNXfEfbKa10ialHMI1iAbEdO');
insert into employee(`name`,nip,id_position,password) values ('Pelaksana','1',(select id from position where name='PELAKSANA' limit 1),'$2a$10$To/okZ7RV.f5c2lNvUBHe.THDiLxaqNXfEfbKa10ialHMI1iAbEdO');
insert into employee(`name`,nip,id_position,password) values ('Manager Teknik','2',(select id from position where name='MANAGER_TEKNIK' limit 1),'$2a$10$To/okZ7RV.f5c2lNvUBHe.THDiLxaqNXfEfbKa10ialHMI1iAbEdO');
insert into employee(`name`,nip,id_position,password) values ('Manager Keuangan','3',(select id from position where name='MANAGER_FINANCE' limit 1),'$2a$10$To/okZ7RV.f5c2lNvUBHe.THDiLxaqNXfEfbKa10ialHMI1iAbEdO');
-- password 123
